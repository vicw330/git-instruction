# A Brief git Note #

### Configurations ###
git config -—global user.name “example”
git config —-global user.email “example@gmail.com”
git config —-global core.editor “vi”

### Initiation ###
git init

### Check Status ###
git status

### Add New Files ###
git add species.sh
git commit -m “started my species study”

### Scheme ###
v1 — v2 — v3 — … — HEAD~2 — HEAD~1 — HEAD

### Check Log FIle ###
git log

### Compare ###
git diff HEAD~1 species.sh
git diff 32d2d4dae species.sh

### Get Things Back: Checkout ###
git checkout HEAD species.sh
git checkout HEAD~1 species.sh

### Ignore ###
vi .gitignore
add: > zoo_1.txt
add: > someDirectory/someFile.txt
use hashtag to comment: # someDirectory2/someFile2.txt

git add .gitignore
git commit -m “ignore zoo_1.txt”

### Point to Server ###
git remote add origin https://github.com/example/questions.git

### Push to Server ###
git push origin master

### Pull from Server ###
git clone https://github.com/example/questions.git
git pull origin master # get other’s contribution

### Conflicts ###
git pull origin master
change them, solve this question
git push origin master

### Revert ###
git revert 32d2d4dae

### Rename ###
git mv zoo_1.txt zoo_100.txt

